'use strict'
/** VARIABLES */
/**
 * @var {Element} bouton L'objet du DOM bouton au click donne ok dans le doc
 */
let bouton;

/** FONCTIONS */

function addEventToElement(){
    console.log('ok');
};

/** PROGRAMME */
// on attend que le DOM soit chargé
document.addEventListener('DOMContentLoaded',()=>{

    console.log('DOM chargé');
    
    bouton = document.querySelector('button');

    bouton.addEventListener('click',addEventToElement);


});