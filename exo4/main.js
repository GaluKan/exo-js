'use strict'

/** VARIABLE */
let info;
let pres;
let cv;
let contacts;
/** FONCTIONS */

function ongletSelect(){
    console.log('clique ok');
    this.classList.add('current');
    
}

/** PROGRAMME */

document.addEventListener('DOMContentLoaded', ()=>{

    console.log('DOM Ok')
    pres = document.querySelector('#presentation');
    info = document.querySelector('#information');
    info.addEventListener('click', ongletSelect);
    pres.addEventListener('click', ongletSelect);
    cv = document.querySelector('#moncv');
    contacts = document.querySelector('#contact');
    cv.addEventListener('click', ongletSelect);
    contacts.addEventListener('click', ongletSelect);

});